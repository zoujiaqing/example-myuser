module app.controller.api.UserController;

import hunt.framework;

import app.repository.UserRepository;
import app.model.User;
import app.message.ResultMessage;
import app.form.UserForm;

class UserController : Controller
{
    mixin MakeController;

    @Action
    Response add(UserForm form)
    {
        // ResultMessage 是要返回的 json 消息体
        auto resultMessage = new ResultMessage;

        // 使用 valid() 方法获取一个校验结果对象
        auto valid = form.valid();
        if (!valid.isValid())
        {
            // 给个错误码
            resultMessage.code = 10001;

            // valid.messages() 方法可以获取所有错误信息，我们这里只简单的获取一个错误进行返回
            foreach (message; valid.messages())
            {
                resultMessage.message = message;
                break;
            }
        }
        else
        {
            auto repository = new UserRepository;

            auto user = new User;
            user.username = form.username;
            user.password = form.password;
            user.email = form.email;

            // 把模型数据保存到数据库
            repository.save(user);

            // 因没有错误就不需要设置错误码，提示添加成功即可
            import std.format : format;

            resultMessage.message = format!"用户( %d )添加成功！"(user.id);
        }

        // 返回结果对象会自动由 JsonResponse 序列化为 json 返回给客户端
        return new JsonResponse(resultMessage);
    }

    @Action
    Reponse del(ulong id)
    {
        //
        auto result = new ResultMessage;
        result.message = "Delete success!";

        return new JsonResponse(result);
    }

    @Action
    Response mod()
    {
        //
        auto result = new ResultMessage;
        result.message = "Modify success!";

        return new JsonResponse(result);
    }

    @Action
    Response detail(ulong id)
    {
        //
        auto result = new ResultMessage;
        result.message = "Modify success!";

        return new JsonResponse(result);
    }

    @Action
    Response list()
    {
        //
        auto result = new ResultMessage;
        result.message = "Modify success!";

        return new JsonResponse(result);
    }
}
