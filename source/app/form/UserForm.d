module app.form.UserForm;

import hunt.framework;

class UserForm : Form
{
    mixin MakeForm;

    @Length(4, 30, "用户名长度必须在 {{min}} 到 {{max}} 位之间。")
    string username;

    @Length(8, 32, "密码长度必须在 {{min}} 到 {{max}} 位之间。")
    string password;
    
    @NotEmpty("Email地址不允许为空。")
    string email;
}
